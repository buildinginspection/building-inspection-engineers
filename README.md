Bill is a licensed Civil and Structural Engineer in Ohio and Kentucky. He is also one of the few Certified Building Inspection Engineers in the United States. He began framing houses when he was just 17 years old. Since then, he’s built two houses in Cincinnati and remodeled countless older homes.

Address: 675 Park Ave, Cincinnati, OH 45246, USA

Phone: 513-773-5832

Website: https://buildinginspectionengineersllc.com/
